import { Routes } from '@angular/router';
import { LayoutComponent } from './full-layout/layout/layout.component';

export const routes: Routes = [
    {
        path: "",
        redirectTo: "page/categories",
        pathMatch: "full",

    },
    {
        path: "",
        component: LayoutComponent,
        children: [
            {
                path: "page",
                loadChildren: () => import("../app/pages/pages.module").then(m => m.PagesModule)
            }
        ]

    },

];
