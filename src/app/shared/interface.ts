export interface CATEGORIES {
    idCategory: string,
    strCategory: string,
    strCategoryThumb: string,
    strCategoryDescription: string
}
export interface CATEGORY_ITEMS {
    idMeal: string,
    price: number,
    strMeal: string,
    strMealThumb: string,
    cartCount: number,
    strCategory: string,


}