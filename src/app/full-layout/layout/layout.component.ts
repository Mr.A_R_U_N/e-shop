import { Component } from '@angular/core';
import { AppContentComponent } from '../app-content/app-content.component';
import { TopBarComponent } from '../top-bar/top-bar.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'shop-layout',
  standalone: true,
  imports: [AppContentComponent, TopBarComponent, CommonModule],
  templateUrl: './layout.component.html',
  styleUrl: './layout.component.scss'
})
export class LayoutComponent {

}
