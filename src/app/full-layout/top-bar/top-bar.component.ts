import { Component } from '@angular/core';
import { CommonService } from '../../core/service/common.service';
import { CATEGORY_ITEMS } from '../../shared/interface';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'shop-top-bar',
  standalone: true,
  imports: [CommonModule,],
  templateUrl: './top-bar.component.html',
  styleUrl: './top-bar.component.scss'
})
export class TopBarComponent {
  public cartItems: CATEGORY_ITEMS[] = []
  constructor(private service: CommonService, private route: Router) {

    this.service.cartItemservice.subscribe((response: CATEGORY_ITEMS[]) => {
      this.cartItems = response;
    })

  }


  showCart = () => {
    this.route.navigate(["/page/cart"])
  }
}
