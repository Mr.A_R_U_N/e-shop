import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { CommonService } from './core/service/common.service';
import { HttpClientModule } from '@angular/common/http';
@Component({
  selector: 'shop-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet,
    HttpClientModule
  ],
  providers: [CommonService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'e-shop';
}
