import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { END_POINTS } from '../api-endpoints/end-points';
import { CATEGORY_ITEMS } from '../../shared/interface';

@Injectable({
  providedIn: 'root'
})

/*
  Angular service for managing common functionalities related to data retrieval, storage, and manipulation.
  Utilizes BehaviorSubject to provide reactive data sharing across components.
  Includes methods for fetching categories, category items, and item details from a remote API.
  Implements cart functionality with methods for adding, updating, and removing items.
  Provides centralized data observables for categories, category items, and cart items for easy component integration.
*/

export class CommonService {

  private BASE_URL = "https://www.themealdb.com/api";

  private categories = new BehaviorSubject([]);
  public categoriesService = this.categories.asObservable();

  public categoryItems: any[] = [];
  private categoryItemsBehaviorSubject = new BehaviorSubject([]);
  public categoryItemService = this.categoryItemsBehaviorSubject.asObservable();

  private cartItems: any[] = [];
  private cartItemsBehaviorSubject = new BehaviorSubject<any>([]);
  public cartItemservice = this.cartItemsBehaviorSubject.asObservable();

  public orders: any[] = [];

  constructor(private http: HttpClient) {
    this.getCategories();
  }

  /*Fetches all categories from the server and stores them in the 'categories' 
  variable for centralized data sharing*/
  getCategories = () => {
    this.http.get(`${this.BASE_URL}${END_POINTS.CATEGORIES}`).subscribe({
      next: (response: any) => {
        if (response.categories?.length) {
          this.categories.next(response.categories);
        } else {
          this.categories.next([]);
        }
      },
      error: (error) => {
        this.categories.next([]); // Set categories to empty array on error
      },
    });
  };


  /* Retrieves category items, either from the existing data or through an API call, 
  and updates the behavior subject for centralized data sharing*/
  getCategoryItems = (category_name: string) => {

    this.categoryItemsBehaviorSubject.next([]);

    const isItemExists = this.categoryItems.find((data: any) => data.category_name == category_name.toLowerCase());
    if (isItemExists?.data?.length) {
      this.categoryItemsBehaviorSubject.next(isItemExists?.data);
      return;
    }

    this.http.get(`${this.BASE_URL}${END_POINTS.GET_CATEGORY_ITEMS}?c=${category_name}`).subscribe({
      next: (response: any) => {
        const mealWithPrice = response.meals.map((meal: any) => {
          meal.price = Math.floor(Math.random() * 900) + 100;
          return meal

        })
        this.categoryItemsBehaviorSubject.next(mealWithPrice);
        this.categoryItems.push({ category_name: category_name.toLowerCase(), data: mealWithPrice })

      },
      error: (error) => {

      }
    });
  }

  getItemInfo(item_id: string | number): Observable<any> {
    return this.http.get(`${this.BASE_URL}${END_POINTS.GET_ITEM_INFO}?i=${item_id}`)
  }

  addToCart = (item: any) => {
    this.cartItems.push(item);
    this.cartItemsBehaviorSubject.next(this.cartItems);
    console.log("CART ITEM", this.cartItems);

  }

  increaseCartItemCount = (item: CATEGORY_ITEMS) => {
    this.cartItems.forEach((data: CATEGORY_ITEMS) => {
      if (data.idMeal == item.idMeal) {
        data.cartCount = item.cartCount;
      }
    })
    console.log("CART ITEM", this.cartItems);


  }

  decreaseCartItemCount = (item: any) => {
    this.cartItems.forEach((data: CATEGORY_ITEMS) => {
      if (data.idMeal == item.idMeal) {
        data.cartCount = item.cartCount;
      }
    })
  }

  removeFromCart = (item: any) => {
    this.cartItems = this.cartItems.filter((data: CATEGORY_ITEMS) => data.idMeal != item.idMeal);
    this.cartItemsBehaviorSubject.next(this.cartItems);
  }
}