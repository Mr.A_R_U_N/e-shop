// Enum representing various API endpoints for convenience
export enum END_POINTS {
    CATEGORIES = "/json/v1/1/categories.php",                // Endpoint for fetching categories
    GET_CATEGORY_ITEMS = "/json/v1/1/filter.php",            // Endpoint for fetching items based on category
    GET_ITEM_INFO = "/json/v1/1/lookup.php",                 // Endpoint for retrieving detailed information about a perticular item
}
