import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryItemsComponent } from './category-items/category-items.component';
import { ItemInfoComponent } from './item-info/item-info.component';
import { CartComponent } from './cart/cart.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "categories",
    pathMatch: "full"
  },
  {
    path: "categories",
    component: CategoriesComponent
  },
  {
    path: "category-items/:category_name",
    component: CategoryItemsComponent
  },
  {
    path: "item-info/:item_id",
    component: ItemInfoComponent
  }
  ,
  {
    path: "cart",
    component: CartComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
