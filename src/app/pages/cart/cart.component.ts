import { Component, OnDestroy } from '@angular/core';
import { CommonService } from '../../core/service/common.service';
import { CATEGORY_ITEMS } from '../../shared/interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shop-cart',
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.scss'
})
export class CartComponent implements OnDestroy {
  public cartItems: CATEGORY_ITEMS[] = [];
  public subscriptions: Subscription[] = [];

  public isPlaceOrder = false;
  public orderArray: any[] = [];
  constructor(private service: CommonService) {

    this.subscriptions.push(
      this.service.cartItemservice.subscribe((response: CATEGORY_ITEMS[]) => {
        if (response.length) {
          this.cartItems = response;
        }
      })
    )
  }

  incrementCount(item: CATEGORY_ITEMS) {
    item.cartCount = item.cartCount
      ? item.cartCount + 1 : 1;
    this.service.increaseCartItemCount(item);
  }

  decrementCount(item: CATEGORY_ITEMS) {

    if (item.cartCount == 1) {
      item.cartCount = 0;
      this.service.removeFromCart(item);
    } else {
      item.cartCount = item.cartCount > 1
        ? item.cartCount - 1 : 1;
      this.service.decreaseCartItemCount(item);

    }

  }

  calculateTotalCost = () => {
    return this.cartItems.reduce((total, item) => {
      return total + item.cartCount * item.price;
    }, 0);
  }

  placeOrderFn = () => {
    this.isPlaceOrder = true;
    this.service.orders.push({ orderId: Math.floor(Math.random() * 900) + 100, data: this.cartItems, total: this.calculateTotalCost() });
    this.orderArray = this.service.orders;
  }

  ngOnDestroy(): void {
    // unsubscribe when leaving the component
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }

}
