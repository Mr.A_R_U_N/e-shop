import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonService } from '../../core/service/common.service';
import { CATEGORIES } from '../../shared/interface';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shop-categories',
  templateUrl: './categories.component.html',
  styleUrl: './categories.component.scss'
})
export class CategoriesComponent implements OnInit, OnDestroy {
  public subscriptions: Subscription[] = [];
  public category_list!: CATEGORIES[];
  constructor(private service: CommonService, private route: Router) { }
  ngOnInit(): void {
    this.getCategories();
  }

  // Retrieving category list for the service
  // Subscribing to the categoriesService to get data and updating the component's category_list property with the received data.
  getCategories = () => {
    this.service.categoriesService.subscribe((data: CATEGORIES[]) => {
      this.category_list = data;
    });
  }

  onClickCategory = (category: CATEGORIES) => {
    this.route.navigate(["/page/category-items", category.strCategory]);
    console.log("CATEGORY", category);


  }

  categoryItemsTrackByFn = (index: number, item: CATEGORIES) => {
    return item.idCategory;
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
