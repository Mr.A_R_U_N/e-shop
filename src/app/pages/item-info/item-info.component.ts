import { Component, OnDestroy, OnInit } from '@angular/core';
import { CommonService } from '../../core/service/common.service';
import { ActivatedRoute } from '@angular/router';
import { CATEGORY_ITEMS } from '../../shared/interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shop-item-info',
  templateUrl: './item-info.component.html',
  styleUrl: './item-info.component.scss'
})

export class ItemInfoComponent implements OnInit, OnDestroy {
  public item_id!: string | number;
  public item_price!: string | number;
  public subscriptions: Subscription[] = [];

  public item_details: any = {};
  constructor(private service: CommonService, private activeRoute: ActivatedRoute) {
    this.item_id = this.activeRoute.snapshot.params['item_id'];
    this.item_price = this.activeRoute.snapshot.queryParams['price'];
  }
  ngOnInit(): void {
    /*
  The code checks if the category of the received item already exists in the service's categoryItems array.
  If found, it searches for the specific item within that category and updates its details with additional information.
  If the category or item is not found, it assigns the received item details directly to the 'item_details' variable.
  Note: JavaScript objects are passed by reference, allowing modifications to reflect in the original data.
*/

    this.subscriptions.push(
      this.service.getItemInfo(this.item_id).subscribe({
        next: (response: any) => {
          if (response?.meals?.length) {

            const findCategoryIndex = this.service.categoryItems.findIndex((category: any) => category.category_name == response.meals[0]?.strCategory?.toLowerCase());
            if (findCategoryIndex != -1) {
              const findItemIndex = this.service.categoryItems[findCategoryIndex].data.findIndex((mealData: any) => mealData.idMeal == response.meals[0].idMeal);

              if (findItemIndex != -1) {
                this.item_details = this.service.categoryItems[findCategoryIndex].data[findItemIndex];
                this.item_details.strInstructions = response.meals[0].strInstructions
              }

            } else {
              this.item_details = response.meals[0];
            }

          }

        },
        error: (error) => {

        }
      })
    )


  }

  addToCart = () => {
    this.item_details.cartCount = 1;
    this.item_details.price = this.item_price;
    this.service.addToCart(this.item_details)
  }

  incrementCount() {
    this.item_details.cartCount += 1;
    this.service.increaseCartItemCount(this.item_details);
  }

  decrementCount() {
    this.item_details.cartCount -= this.item_details.cartCount > 1 ? 1 : 0;
    this.service.decreaseCartItemCount(this.item_details);
  }
  ngOnDestroy(): void {
    // unsubscribe when leaving the component
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }
}
