import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryItemsComponent } from './category-items/category-items.component';
import { ItemInfoComponent } from './item-info/item-info.component';
import { CartComponent } from './cart/cart.component';

@NgModule({
  declarations: [
    CategoriesComponent,
    CategoryItemsComponent,
    ItemInfoComponent,
    CartComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    HttpClientModule],
})
export class PagesModule { }
