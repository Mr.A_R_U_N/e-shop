import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../core/service/common.service';
import { CATEGORY_ITEMS } from '../../shared/interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shop-category-items',
  templateUrl: './category-items.component.html',
  styleUrl: './category-items.component.scss'
})
export class CategoryItemsComponent implements OnInit, OnDestroy {

  public category_name!: string;
  public category_items!: CATEGORY_ITEMS[];
  public subscriptions: Subscription[] = [];

  constructor(private activeRoute: ActivatedRoute, private route: Router, private service: CommonService) {
    this.category_name = this.activeRoute.snapshot.params['category_name'];
    service.getCategoryItems(this.category_name);
  }

  ngOnInit(): void {

    this.subscriptions.push(
      this.service.categoryItemService.subscribe((response: CATEGORY_ITEMS[]) => {
        if (response.length) {
          this.category_items = response;
        }
      })
    )

  }

  onClickItem = (item: CATEGORY_ITEMS) => {
    this.route.navigateByUrl(`/page/item-info/${item.idMeal}?price=${item.price}`)
  }

  categoryItemsTrackByFn = (index: number, item: CATEGORY_ITEMS) => {
    return item.idMeal;
  }

  addToCart = (item: CATEGORY_ITEMS) => {
    item.cartCount = 1;
    item.strCategory = this.category_name;
    this.service.addToCart(item)
  }

  incrementCount(item: CATEGORY_ITEMS) {
    item.cartCount = item.cartCount
      ? item.cartCount + 1 : 1;
    this.service.increaseCartItemCount(item);
  }

  decrementCount(item: CATEGORY_ITEMS) {
    item.cartCount = item.cartCount > 1
      ? item.cartCount - 1 : 1;
    this.service.decreaseCartItemCount(item);

  }

  ngOnDestroy(): void {
    // unsubscribe when leaving the component
    this.subscriptions.forEach((sub: Subscription) => sub.unsubscribe());
  }


}
